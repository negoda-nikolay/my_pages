//PLUGIN 
(function ($) {
    //methods:
    var methods = {
        //init: function () {},

        select: function () {
            return this.removeClass("free_table").addClass("selected_table");
        },

        deselect: function () {
            return this.removeClass("selected_table").addClass("free_table");
        },

        book: function () {
            return this.removeClass("selected_table").addClass("booked_table");
        }

        , clear: function () {
            return this.removeClass("selected_table booked_table").addClass("free_table");
        }
    };

    //call some method:
    $.fn.keith_book_plugin = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error("Method '" + method + "' isn't exist");
        }
    };
})(jQuery);
//END PLUGIN