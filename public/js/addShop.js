$(function () {
    $('#datetimepicker-start').datetimepicker( {pickDate: false, language: 'ru'} );
    $('#datetimepicker-end').datetimepicker( {pickDate: false, language: 'ru'} );
    $('.date').datepicker({});

    function previewImage(file) {
        var shopPlan = document.getElementById("shop-plan-img");
        var imageType = /image.*/;

        if (!file.type.match(imageType)) {
            throw "File Type must be an image";
        }

        var shopPlanImg = document.createElement("div");
        shopPlanImg.classList.add('thumbnail');

        var img = document.createElement("img");
        img.file = file;
        shopPlanImg.appendChild(img);
        shopPlan.appendChild(shopPlanImg);

        // Using FileReader to display the image content
        var reader = new FileReader();
        reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
        reader.readAsDataURL(file);
    }

    var uploadfiles = document.querySelector('#inputShopPlan');
    uploadfiles.addEventListener('change', function () {
        var files = this.files;
        for(var i=0; i<files.length; i++){
            previewImage(this.files[i]);
        }

    }, false);

    function previewImage2(file) {
        var shopPlan = document.getElementById("shop-avatar-img");
        var imageType = /image.*/;

        if (!file.type.match(imageType)) {
            throw "File Type must be an image";
        }

        var shopPlanImg = document.createElement("div");
        shopPlanImg.classList.add('thumbnail');

        var img = document.createElement("img");
        img.file = file;
        shopPlanImg.appendChild(img);
        shopPlan.appendChild(shopPlanImg);

        // Using FileReader to display the image content
        var reader = new FileReader();
        reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
        reader.readAsDataURL(file);
    }

    var uploadfiles2 = document.querySelector('#inputShopAvatar');
    uploadfiles2.addEventListener('change', function () {
        var files = this.files;
        for(var i=0; i<files.length; i++){
            previewImage2(this.files[i]);
        }

    }, false);

});

