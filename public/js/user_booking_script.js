$(document).ready(function () {
    //Date and Time
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'ru'
        });
    });

    //Select and Deselect Tables
    //Using keith_book_plugin
    $(function () {
        //Click on any table
        $('#plan li div').on("click", function () {
            $('#error_table_not_selected').hide();
            if ($(this).hasClass("free_table")) {
                $(this).keith_book_plugin('select');
            } else if ($(this).hasClass("selected_table")) {
                $(this).keith_book_plugin('deselect');
            }
        });

        //Click Book It Button
        $('#book_it_button').on('click', function () {
            if ($("#plan li div.selected_table").length) {
                $("#plan li div.selected_table").keith_book_plugin("book");
            }
            else {
                $('#error_table_not_selected').show();
            }
        });

        //Clear Selections
        $("#clear_selections_button").on('click', function () {
            $('#error_table_not_selected').hide();
            $("#plan li div").keith_book_plugin("clear");
        });
    });
});