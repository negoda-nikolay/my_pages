<?php

namespace Goodevening\Mail;

class Message
{
    private $body;
    private $charset = 'UTF-8';

    private $to = array();
    private $from;
    private $subject;
    private $message;
    private $headers = array();

    public function __construct($from = null, $to = null, $subject = null, $message = null, $headers = null)
    {
        $this->from = $from;
        $this->subject = $subject;
        $this->message = $message;

        if(is_array($to))
            $this->to = $to;
        else
            $this->to[] = $to;

        if($headers === null)
            $this->headers = $this->generateHeaders();
        else
            $this->headers = $headers;
    }

    private function generateBody()
    {
        $body = implode("\r\n", $this->headers) . "\r\n\r\n";
        $body .= $this->message . "\r\n";

        $this->body = $body;
    }

    private function generateHeaders()
    {
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/html; charset=UTF-8";

        $headers[] = "From: $this->from <$this->from>";
        $headers[] = "Date: " . date("D, d M Y H:i:s") . " UT";
        $headers[] = 'Subject: =?' . $this->charset . '?B?' . base64_encode($this->subject) . "=?=";

        $headers[] = "To: ".$this->getToString();

        return $headers;
    }

    public function getBody()
    {
        if (!($this->body))
            $this->generateBody();
        return $this->body;
    }

    public function setFrom($from)
    {
        $this->from = $from;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function setTo($to)
    {
        $this->to = $to;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getToString()
    {
        return implode(",", $this->getTo());
    }
}