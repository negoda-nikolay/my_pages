<?php

namespace Goodevening\Mail\Transport;

use Goodevening\Mail\Message;

interface TransportInterface
{
    public function send(Message $message);
}