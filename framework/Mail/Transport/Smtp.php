<?php

namespace Goodevening\Mail\Transport;

use Goodevening\Mail\Message;
use Goodevening\Mail\MailException;

class Smtp implements TransportInterface
{

    private $smtpUsername;
    private $smtpPassword;
    private $smtpHost;
    private $smtpPort;

    public function __construct($smtpUsername = null, $smtpPassword = null, $smtpHost = null, $smtpPort = null)
    {
        $this->smtpUsername = $smtpUsername;
        $this->smtpPassword = $smtpPassword;
        $this->smtpHost = $smtpHost;
        $this->smtpPort = $smtpPort;
    }

    function send(Message $message)
    {
        try
        {
            $socket = @fsockopen($this->smtpHost, $this->smtpPort, $errorNumber, $errorDescription, 30);
            if(!$this->parseServer($socket, "220"))
                throw new MailException('Connection error');

            fputs($socket, "HELO ".$_SERVER["SERVER_NAME"]."\r\n");
            if(!$this->parseServer($socket, "250"))
                throw new MailException('Error of command HELO');

            fputs($socket, "AUTH LOGIN\r\n");
            if(!$this->parseServer($socket, "334"))
                throw new MailException('Error of command AUTH LOGIN');

            fputs($socket, base64_encode($this->smtpUsername) . "\r\n");
            $this->parseServer($socket, "334");

            fputs($socket, base64_encode($this->smtpPassword) . "\r\n");
            if(!$this->parseServer($socket, "235"))
                throw new MailException('Bad username or password');

            fputs($socket, "MAIL FROM: <".$this->smtpUsername.">\r\n");
            if(!$this->parseServer($socket, "250"))
                throw new MailException('Error of command MAIL FROM');

            foreach($message->getTo() as $to)
            {
                fputs($socket, "RCPT TO: <" . $to . ">\r\n");
                if(!$this->parseServer($socket, "250"))
                    throw new MailException('Error of command RCPT TO');
            }

            fputs($socket, "DATA\r\n");
            if(!$this->parseServer($socket, "354"))
                throw new MailException('Error of command DATA');

            fputs($socket, $message -> getBody() ."\r\n.\r\n");
            if(!$this->parseServer($socket, "250"))
                throw new MailException('E-mail did not sent');

            fputs($socket, "QUIT\r\n");
            fclose($socket);
        }
        catch (MailException $e)
        {
            echo $e->getMessage();
            return  $e->getMessage();
        }
        return true;
    }

    private function parseServer($socket, $response)
    {
        $responseServer = fgets($socket, 256);
        if (!(substr($responseServer, 0, 3) == $response))
        {
            return false;
        }
        return true;
    }

    public function setUsername($username)
    {
        $this->smtpUsername = $username;
    }

    public function setPassword($smtpPassword)
    {
        $this->smtpPassword = $smtpPassword;
    }

    public function setHost($smtpHost)
    {
        $this->smtpHost = $smtpHost;
    }

    public function setPort($smtpPort)
    {
        $this->smtpPort = $smtpPort;
    }
}