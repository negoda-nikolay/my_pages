<?php

namespace Goodevening\Mail\Transport;

use Goodevening\Mail\Message;

class Sendmail implements TransportInterface
{
    public function send(Message $message)
    {
        return (mail($message->getTo(), $message->getSubject(), $message->getMessage(), $message->getHeaders()));
    }
}