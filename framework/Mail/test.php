<?php

namespace Goodevening\Mail;

use Goodevening\Mail\Message;
use Goodevening\Mail\Transport\Smtp;
use Goodevening\Mail\Transport\Sendmail;

/*
    $message = new Message();
    $message -> setFrom();
    $message -> setTo();
    $message -> setSubject();
    $message -> setMessage();
    $message -> setHeaders();
    $message -> setParameters();
*/

$message = new Message('from_mail@example.com', 'to_mail@example.com', 'the subject', 'the message');

/*
    $mailSMTP = new Smtp();
    $mailSMTP -> setUsername();
    $mailSMTP -> setPassword();
    $mailSMTP -> setHost();
    $mailSMTP -> setPort();
*/

$mailSMTP = new Smtp('testformail2016@yandex.ru', 'testformail20160', 'ssl://smtp.yandex.ru', 465);
$mailSMTP->send($message);

//$mailSendmail = new Sendmail();
//$mailSendmail->send($message);