<?php

namespace Goodevening\Application;

use Goodevening\Acl\Acl;

use Goodevening\Auth\Auth;
use Goodevening\Router\Router;
use Goodevening\Session\Session;

class Bootstrap
{
    public static function runApplication()
    {
        $auth = new Auth();

        //$auth->authenticate(['email' => 'petro@mail.com', 'password' => '123']);
        foreach ($auth->getSocialUris() as $socialName => $link) {
            echo '<a href="' . $link . '" > Sing in via ' . $socialName . '</a><hr>';
        }

        $auth->authenticate(['social']);
        //$auth->logout();
        var_dump($auth->isLogged());
        $us = Session::getSession()->user;
        var_dump($us);

/*        $acl = new Acl();
       $acl->denyPrivilege('admin', 'myResouce', 'edit')
           ->denyPrivilege('admin2', '2myResouce', '2edit')
           ->denyPrivilege('admi3', '3myResouce', '3edit')
           ->denyPrivilege('admi4', '4myResouce', '4edit')
           ->denyPrivilege('admi5', '5myResouce', '5edit');

      //  $acl->denyPrivilege('admin', 'myResouce3', 'edit');
        var_dump($acl->isAllowed('admin3', 'myResouce2', 'edit'));
        var_dump($acl->isAllowed('admi5', '5myResouce', '5edit'));
        var_dump($acl->isAllowed('guest', 'AdminController', 'index'));
        var_dump($acl->isAllowed('guest', 'AdminController', 'create'));
        var_dump($acl->isAllowed('guest', 'AdminController', 'delete'));
        var_dump($acl->isAllowed('guest', 'AdminController', 'rename'));
        var_dump($acl->isAllowed('guest', 'AdminController', 'lock'));*/

        /*     $acl->addRole('newTestRole');
    /*         var_dump($acl->isAllowed('guest', 'LocationsController', 'index'));
               $acl->deny('user', 'SomeCon', 'asds');
               $acl->deny('user', 'SomeCon', 'dddddd');*/
        $route = new Router();
        $route->start();
    }

}