<?php
namespace Goodevening\Db;

abstract class DbAdapterAR
{
    static protected $dbh;
    static protected $tableName;
    static protected $fields;
    protected $params = [];

    public function __get($property)
    {
        try {
            if (array_key_exists($property, $this->params)) {
                return $this->params[$property];
            } else {
                throw new \Exception("Property $property not exist");
            }
        } catch (\Exception $e) {
            die(" Error " . $e->getMessage() . " Line " . $e->getLine() . " File " . $e->getFile());
        }
    }

    public function __set($property, $value)
    {
        $this->params[$property] = $value;
    }

    public function __unset($property)
    {
        unset($this->params[$property]);
    }

    public function __isset($property)
    {
        return isset($this->params[$property]);
    }

    /**
     * @param array $findWhere
     * @param array $fields
     * @return array
     */
    public static function find(array $findWhere, array $fields)
    {
        self::$dbh = DbConnect::getConnection();
        $fields = join(', ', $fields);
        $table = static::$tableName;

        $columnName = array_keys($findWhere);
        $columnValue = array_values($findWhere);

        $where = "";
        $countFields = count($findWhere);
        for ($i = $countFields - 1; $i >= 0; $i--) {
            $where .= $columnName[$i] . "=:v$i AND ";
        }
        //$where = substr($where, 0, -5);
        $where = rtrim($where, " AND ");

        $query = "SELECT " . $fields . " FROM " . $table . " WHERE " . $where;
        $sth = self::$dbh->prepare($query);

        for ($i = $countFields - 1; $i >= 0; $i--) {
            $sth->bindValue(':v' . $i, $columnValue[$i]);
        }

        self::execQuery($sth);
        $res = $sth->fetchAll(\PDO::FETCH_ASSOC);

        try {
            if (!$res) {
                return false;
                // throw new DbExceprion("Not found row ");
            }
        } catch (\Exception $e) {
            die(" Error " . $e->getMessage() . " Line " . $e->getLine() . " File " . $e->getFile());
        }
        $objects = [];
        foreach ($res as $val) {
            $class = static::class;
            $objects[] = new $class();
        }
        for ($i = 0; $i <= count($res) - 1; $i++) {
            foreach ($res[$i] as $k => $v) {
                $objects[$i]->$k = $v;
            }
        }
        return $objects;
    }

    /**
     * INSERT new row in database or UPDATE if row exist
     */
    public function save()
    {
        self::$dbh = DbConnect::getConnection();
        $table = static::$tableName;
        $fields = array_keys($this->params);
        $values = array_values($this->params);
        $fieldsHolder = '';
        $valueHolder = '';
        $countFields = count($this->params);

        //if row exist
        if (array_key_exists('id', $this->params)) {
            for ($i = $countFields - 1; $i >= 0; $i--) {
                $fieldsHolder .= "$fields[$i]=:v$i, ";
            }
            //$fieldsHolder = substr($fieldsHolder, 0, -2);
            $fieldsHolder = rtrim($fieldsHolder, ", ");

            $query = 'UPDATE ' . $table . " SET " . $fieldsHolder . " WHERE id=" . $this->params['id'];
            $sth = self::$dbh->prepare($query);

            for ($i = $countFields - 1; $i >= 0; $i--) {
                $sth->bindValue(':v' . $i, $values[$i]);
            }

        } else {
            for ($i = $countFields - 1; $i >= 0; $i--) {
                $fieldsHolder .= "$fields[$i], ";
                $valueHolder .= ":v" . $i . ", ";
            }

            //$fieldsHolder = substr($fieldsHolder, 0, -2);
            //$valueHolder = substr($valueHolder, 0, -2);
            $fieldsHolder = rtrim($fieldsHolder, ", ");
            $valueHolder = rtrim($valueHolder, ", ");

            $query = "INSERT INTO " . $table . " (" . $fieldsHolder . ") VALUES (" . $valueHolder . ")";
            $sth = self::$dbh->prepare($query);

            for ($i = $countFields - 1; $i >= 0; $i--) {
                $sth->bindValue(':v' . $i, $values[$i]);
            }
        }
        $this->execQuery($sth);
    }

    /**
     * DELETE row in database if id exist
     */
    public function delete()
    {
        self::$dbh = DbConnect::getConnection();
        $table = static::$tableName;

        $query = "DELETE FROM $table WHERE id=" . $this->params['id'];
        $sth = self::$dbh->prepare($query);

        $this->execQuery($sth);
    }

    /**
     * Executes a prepared statement
     * @param $sth
     * @throws \Exception
     */
    private static function execQuery($sth)
    {
        try {
            if (!$sth->execute()) {
                var_dump($sth->errorInfo());
                throw new \Exception("Can't perform database operations");
            }
        } catch (\Exception $e) {
            die(" Error " . $e->getMessage() . " Line " . $e->getLine() . " File " . $e->getFile());
        }
    }
}
