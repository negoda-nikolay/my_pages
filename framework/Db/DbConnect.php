<?php

namespace Goodevening\Db;

class DbConnect
{
    private $connectStr;
    private static $hendler = null;

    private function __construct()
    {
        $this->connectStr = require 'application/configs/db.local.php';
        try {
            self::$hendler = new \PDO(
                $this->connectStr["dsn"],
                $this->connectStr['username'],
                $this->connectStr['password']
            );
        } catch (\Exception $e) {
            die(" Error " . $e->getMessage() . " Line " . $e->getLine() . " File " . $e->getFile());
        }
    }

    private function __clone()
    {
    }

    public static function getConnection()
    {
        if (!isset(self::$hendler)) {
            new self;
        }
        return self::$hendler;
    }

}