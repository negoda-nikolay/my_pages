<?php
namespace Goodeveningproj\Model;

use Goodevening\Db\DbAdapterAR;

class TableType extends DbAdapterAR
{
    protected static $tableName = 'table_type';
}