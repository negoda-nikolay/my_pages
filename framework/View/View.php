<?php

namespace Goodevening\View;

class View
{
    public function render($content, $template = 'layout.phtml', $data = null)
    {
        if (is_array($data)) {
            extract($data);
        }
        ob_start();
        require_once 'application/Views/' . $content;
        $page = ob_get_clean();
        require_once 'application/Layouts/' . $template;
    }
}
