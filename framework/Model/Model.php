<?php
/**
 * Created by PhpStorm.
 * User: phpstudent
 * Date: 2/5/16
 * Time: 4:39 PM
 */

namespace Goodevening\Model;

use Validator\Validator;

class Model
{
    public $rules = [];
    public $errorArr = [];

    public function load($attributes)
    {
        $isLoad = false;
        foreach ($attributes as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
                $isLoad = true;
            }
        }
        return $isLoad;
    }

    public function validate()
    {
        if (empty($this->rules)) {
            $this->errorArr['Global Errors'][] = 'Your rules is empty';
            $this->outputErrors();
            return false;
        }

        foreach ($this->rules as $rule) {
            $fields = $rule[0];
            $nameRule = $rule[1];
            $params = isset($rule[2]) ? $rule[2] : null;

            $validator = new Validator($nameRule, $params);
            foreach ($fields as $field) {
                if (!$validator->isValid($this->$field)) {
                    $this->errorArr[$field][] = $validator->getErr($this->$field);
                }
            }
        }

        if (!empty($this->errorArr)) {
            $this->outputErrors();
            return false;
        }

        return true;
    }


    public function outputErrors()
    {
        ob_start();
        foreach ($this->errorArr as $errorType => $arrayErrors) {
            echo '<h3>' . $errorType . ': </h3>';
            if (!is_array($arrayErrors)) {
                echo '<p>' . $arrayErrors . ' </p>';
            } else {
                foreach ($arrayErrors as $name => $error) {
                    echo '<p>' . $error . ' </p>';
                }
            }

        }
        $ob = ob_get_clean();
        echo $ob;
    }
}