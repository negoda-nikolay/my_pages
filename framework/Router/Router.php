<?php

namespace Goodevening\Router;

use Application\Controllers\Err404Controller;

class Router
{
    public $actionName = 'indexAction';
    public $controllerName = 'Err404Controller';
    public $params = [];
    private $routes = [];

    /**
     * get routers from config
     */
    public function __construct()
    {
        $pathToConf = 'application/configs/routers.config.php';
        $this->routes = require $pathToConf;
    }

    /**
     * return array with uri path
     * like controller/action/par1/val1/par2/val2/....
     */
    private function getUri()
    {
        return isset($_SERVER['REQUEST_URI']) ? trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/') : null;
    }

    /**
     * @param $params
     * @return array
     */
    private function getParams($params)
    {
        $return = [];
        for ($i = 0; $i < count($params); $i++) {
            $return[$params[$i]] = $params[++$i];
        }
        $return = array_merge($return, $_GET);
        return $return;
    }

    private function parsUrl()
    {
        $uri = $this->getUri();
        foreach ($this->routes as $pattern => $paths) {
            if (preg_match("~$pattern~", $uri)) {
                $controllerActionParams = preg_replace("~$pattern~", $paths, $uri);
                $controllerAndAction = explode('/', $controllerActionParams);
                $this->controllerName = ucfirst(array_shift($controllerAndAction)) . 'Controller';
                $this->actionName = array_shift($controllerAndAction) . 'Action';
                $this->params = $controllerAndAction;
                break;
            }
        }
    }

    public function start()
    {
        $this->parsUrl();
        try {
            $this->controllerName = 'Application\\Controllers\\' . $this->controllerName;
            if (count($this->params) % 2) {
                throw new \Exception();
            }
            $this->params = $this->getParams($this->params);

//            echo 'Controller: ' . $this->controllerName . '<br> Action: ' . $this->actionName;
 //           var_dump($this->params);

            if (!class_exists($this->controllerName)) {
                throw new \Exception("Controller $this->controllerName not found");
            }

            $controller = new $this->controllerName;
            if (method_exists($controller, $this->actionName)) {
                $controller->{$this->actionName}();
            } else {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            //for test
            echo " Error " . $e->getMessage() . " Line " . $e->getLine() . " File " . $e->getFile();
           /* var_dump($this->params);*/

            $controller = new Err404Controller();
            $controller->indexAction();
        }
    }
}