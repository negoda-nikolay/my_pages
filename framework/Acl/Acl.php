<?php

namespace Goodevening\Acl;

class Acl
{
    protected $config;
    protected $deny;
    protected $allow;

    const ADD_RULE = 'ADD_RULE';
    const DENY_RULE = 'DENY_RULE';

    public function __construct()
    {
        $this->config = self::loadAclConfig();
        $this->deny = $this->config['deny'];/*
        $this->allow = $this->config['allow'];*/
    }

    /**
     * Returns array with acl config
     *
     * @param string $path
     * @return array
     */
    private static function loadAclConfig($path = 'application/configs/acl.conf.php')
    {
        return require $path;
    }

    /**
     * If failure returns null
     * @param string $path
     * @return int
     */
    /*
        private function saveAclConfig($path = 'application/configs/acl.conf.php')
        {
            $newConf['allow'] = $this->allow;
            $newConf['deny'] = $this->deny;
            $content = "<?php" . PHP_EOL . "return " . var_export($newConf, true) . ";";
            return file_put_contents($path, $content);
        }*/

    /**
     * @param $role
     * @param $resource
     * @param $privilege
     * @return bool
     */
    public function isAllowed($role, $resource, $privilege)
    {
        if (array_key_exists($role, $this->deny)) {
            if (array_key_exists($resource, $this->deny[$role])) {
                if (in_array($privilege, $this->deny[$role][$resource])) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @param $role
     * @return $this
     */
    public function addRole($role)
    {
        if (!array_key_exists($role, $this->deny)) {
            $this->deny[$role] = [];
        }
        /*        if (!array_key_exists($role, $this->allow)) {
                    $this->allow[$role] = [];
                }*/
        return $this;
    }

    /**
     * Method add new role if it dose not exist
     * @param $role
     * @param $resource
     * @return $this
     */
    public function addResource($role, $resource)
    {
        if (!$this->roleExist($role)) {
            $this->addRole($role);
        }
        if (!array_key_exists($resource, $this->deny[$role])) {
            $this->deny[$role][$resource] = [];
        }
        /*        if (!array_key_exists($resource, $this->allow[$role])) {
                    $this->allow[$role][$resource] = [];
                }*/
        return $this;
    }

    /**
     * @param $role
     * @return bool
     */
    private function roleExist($role)
    {
        if (/*array_key_exists($role, $this->allow) && */array_key_exists($role, $this->deny)) {
            return true;
        }
        return false;
    }

    /**
     * Method also add new resource and role if they does not exist
     *
     * @param $role
     * @param $resource
     * @param $privilege
     * @return $this
     */

    /*    public function addPrivilege($role, $resource, $privilege)
        {
            return $this->setRule(self::ADD_RULE, $role, $resource, $privilege);
        }*/

    /**
     * Method also add new resource and role if they does not exist
     *
     * @param $role
     * @param $resource
     * @param $privilege
     * @return $this|void
     */
    public function denyPrivilege($role, $resource, $privilege)
    {
        return $this->setRule(self::DENY_RULE, $role, $resource, $privilege);
    }

    /**
     * $operation can be ADD_RULE or DENY_RULE
     *
     * @param $operation
     * @param $role
     * @param $resource
     * @param $privilege
     * @return $this|void
     */
    private function setRule($operation, $role, $resource, $privilege)
    {
        $this->addResource($role, $resource);

        $rules = [];
        switch ($operation) {
            case self::ADD_RULE:
                $rules = &$this->allow;
                break;
            case self::DENY_RULE:
                $rules = &$this->deny;
                break;
        }

        if (!in_array($privilege, $rules[$role][$resource])) {
            $rules[$role][$resource][] = &$privilege;
            unset($privilege, $rules);
            //$this->saveAclConfig();
            return $this;
        }
        return $this;
    }
}
