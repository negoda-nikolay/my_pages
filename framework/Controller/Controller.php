<?php

namespace Goodevening\Controller;

use Goodevening\View\View;

abstract class Controller
{
    protected $model;
    protected $view;

    public function __construct()
    {
        $this->view = new View();
    }

    protected function indexAction()
    {
        echo "Class: " . __CLASS__ . " <br /> Method: " . __METHOD__;
    }
}