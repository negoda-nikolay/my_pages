<?php

/**
 * Created by PhpStorm.
 * User: zyuzka
 * Date: 25.01.2016
 * Time: 21:45
 */
namespace Goodevening\Validator;


class Validator
{
    public $rule;

    public function __construct($nameRule, $params = null)
    {
        $this->addRule($nameRule, $params);
    }

    public function addRule($nameRule, $params)
    {
        $className = ucfirst($nameRule) . 'Rule';
        $pathClass = 'Validator\\Rules\\' . $className;

        if (class_exists($pathClass)) {
            $this->rule = new $pathClass($params);
        } else {
            die('Rule not found.');
        }
    }

    public function isValid($value)
    {
        return $this->rule->validate($value);
    }

    public function getErr($value)
    {
        return $this->rule->getError($value);
    }
}