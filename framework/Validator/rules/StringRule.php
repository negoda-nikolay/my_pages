<?php
namespace Validator\Rules;
/**
 * Created by PhpStorm.
 * User: zyuzka
 * Date: 26.01.2016
 * Time: 2:25
 */
class StringRule extends AbstractRule
{
    public $errMessage = 'not a string';

    public function validate($value)
    {
        return is_string($value) ? true : false;
    }
}