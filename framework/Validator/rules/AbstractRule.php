<?php
/**
 * Created by PhpStorm.
 * User: zyuzka
 * Date: 26.01.2016
 * Time: 3:32
 */

namespace Validator\Rules;


abstract class AbstractRule
{
    public $errMessage = 'your value not invalid!';

    final public function getError($value)
    {
        return $value . ' ' . $this->errMessage;
    }

    abstract public function validate($value);
}