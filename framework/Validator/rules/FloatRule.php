<?php
/**
 * Created by PhpStorm.
 * User: zyuzka
 * Date: 26.01.2016
 * Time: 12:56
 */

namespace Validator\Rules;


class FloatRule extends AbstractRule
{
    public $errMessage = 'not a float';

    public function validate($value)
    {
        return is_float($value) ? true : false;
    }

}