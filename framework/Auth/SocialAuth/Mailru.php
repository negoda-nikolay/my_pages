<?php
namespace Goodevening\Auth\SocialAuth;

class Mailru extends AbstractSocialAdapter
{
    public function __construct($adapterParams)
    {
        parent::__construct($adapterParams);

        $this->fieldsAlias = [
            'name' => 'first_name',
            'socialId' => 'uid',
            'socialInfo' => 'pic_big',
            'email' => 'email',
        ];
    }

    /**
     * Return array with url and params, required for authentication
     *
     * @return array
     */
    protected function getAuthParam()
    {
        return [
            'authUrl' => 'https://connect.mail.ru/oauth/authorize',
            'params' => [
                'client_id' => $this->clientId,
                'response_type' => 'code',
                'redirect_uri' => $this->redirectUri,
            ],
        ];
    }

    public function authenticate()
    {
        if (isset($_GET['code'])) {
            $params = [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'grant_type' => 'authorization_code',
                'code' => $_GET['code'],
                'redirect_uri' => $this->redirectUri,
            ];
            $urlToken = 'https://connect.mail.ru/oauth/token';

            //get token from mail.ru
            $tokenInfo = $this->postRequest($urlToken, $params);

            //use api mail.ru for get user information
            if (isset($tokenInfo['access_token'])) {
                $sign = md5("app_id={$this->clientId}method=users.getInfosecure=1session_key={$tokenInfo['access_token']}{$this->clientSecret}");
                $params = [
                    'method' => 'users.getInfo',
                    'secure' => '1',
                    'app_id' => $this->clientId,
                    'session_key' => $tokenInfo['access_token'],
                    'sig' => $sign,
                ];

                $urlApps = 'http://www.appsmail.ru/platform/api';
                $userInfo = $this->getRequest($urlApps, $params)[0];
                $this->fillUserInfo($userInfo);
                return true;
            }
        }
        return false;
    }
}
