<?php
namespace Goodevening\Auth\SocialAuth;

class Yandex extends AbstractSocialAdapter
{
    public function __construct($adapterParams){
        parent::__construct($adapterParams);

        $this->fieldsAlias = [
            'name' => 'first_name',
            'socialId' => 'id',
            'socialInfo' => 'default_avatar_id',
            'email' => 'default_email',
        ];
    }
    /**
     * Return array with url and params, required for authentication
     *
     * @return array
     */
    protected function getAuthParam()
    {
        return [
            'authUrl' => 'https://oauth.yandex.ru/authorize',
            'params' => [
                'client_id' => $this->clientId,
                'response_type' => 'code',
                'display'       => 'popup',
                'redirect_uri' => $this->redirectUri,
            ],
        ];
    }

    public function authenticate(){
        if (isset($_GET['code'])) {
            $params = [
                'grant_type' => 'authorization_code',
                'code' => $_GET['code'],
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
            ];

            //get token
            $tokenInfo = $this->postRequest('https://oauth.yandex.ru/token', $params);

            if (isset($tokenInfo['access_token'])) {
                $params = [
                    'format' => 'json',
                    'oauth_token' => $tokenInfo['access_token']
                ];
                //use yandex api for get user info
                $userInfo = $this->getRequest('https://login.yandex.ru/info', $params);

                //convert avatar id to correct uri
                $userInfo['default_avatar_id'] = "https://avatars.yandex.net/get-yapic/{$userInfo['default_avatar_id']}/islands-200";
                if (isset($userInfo['id'])) {
                    $this->fillUserInfo($userInfo);
                    return true;
                }
            }
        }
        return false;
    }
}
