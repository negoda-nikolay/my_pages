<?php
namespace Goodevening\Auth\SocialAuth;

class Google extends AbstractSocialAdapter
{
    public function __construct($adapterParams){
        parent::__construct($adapterParams);

        $this->fieldsAlias = [
            'name' => 'name',
            'socialId' => 'id',
            'socialInfo' => 'picture',
            'email' => 'email',
        ];
    }

    /**
     * Return array with url and params, required for authentication
     *
     * @return array
     */
    protected function getAuthParam()
    {
        return [
            'authUrl' => 'https://accounts.google.com/o/oauth2/auth',
            'params' => [
                'client_id' => $this->clientId,
                'response_type' => 'code',
                'redirect_uri' => $this->redirectUri,
                'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
            ],
        ];
    }

    public function authenticate(){
        if (isset($_GET['code'])) {
            $params = [
                'grant_type' => 'authorization_code',
                'code' => $_GET['code'],
                'redirect_uri' => $this->redirectUri,
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
            ];

            //get token
            $tokenInfo = $this->postRequest('https://accounts.google.com/o/oauth2/token', $params);

            if (isset($tokenInfo['access_token'])) {
                $params['access_token'] = $tokenInfo['access_token'];

                //use google api for get user info
                $userInfo = $this->getRequest('https://www.googleapis.com/oauth2/v1/userinfo', $params);

               if (isset($userInfo['id'])) {
                    $this->fillUserInfo($userInfo);
                    return true;
                }
            }
        }
        return false;
    }
}
