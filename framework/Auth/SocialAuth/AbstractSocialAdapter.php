<?php

namespace Goodevening\Auth\SocialAuth;

use Goodevening\Session\Session;

abstract class AbstractSocialAdapter
{
    protected $clientId;
    protected $clientSecret;
    protected $redirectUri;

    /**
     * Must contains alias for keys in $userSocialInfo array
     *
     * @var array
     */
    protected $fieldsAlias = [];

    /**
     * Contains information about user from social page
     * @var array
     */
    protected $userSocialInfo = [
        'name' => '',
        'socialId' => '',
        'socialInfo' => '',
        'email' => '',
    ];

    protected $config;

    public function __construct($adapterParams)
    {
        if (is_array($adapterParams)) {
            $this->clientId = $adapterParams['clientId'];
            $this->clientSecret = $adapterParams['clientSecret'];
            $this->redirectUri = $adapterParams['redirectUri'];
        }
    }

    /**
     * Must return array with url and params, required for authentication
     * Array keys 'authUrl' and 'params' (Watch documentation for your provider)
     *
     * @return array
     */
    abstract protected function getAuthParam();

    /**
     * Must Fill out $userSocialInfo array user information
     *
     * @return bool
     */
    abstract public function authenticate();

    /**
     * Make link to authorize via social network
     *
     * @return string
     */
    public function getAuthUri()
    {
        $arrayUrl = $this->getAuthParam();
        return $arrayUrl['authUrl'] . "?" . urldecode(http_build_query($arrayUrl['params']));
    }

    /**
     * Do post request to url with $params
     *
     * @param $url
     * @param $params
     * @return array
     */
    protected function postRequest($url, array $params)
    {
        $params = urldecode(http_build_query($params));

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);

        return (array)json_decode($result);
    }

    /**
     * Do get request to url with $params
     *
     * @param $url
     * @param array $params
     * @return array
     */
    protected function getRequest($url, array $params)
    {
        $result = file_get_contents($url . '?' . urldecode(http_build_query($params)));
        return (array)json_decode($result, true);
    }

    /**
     * fillUserInfo and write to session with key 'user'
     *
     * @param $userInfo
     */
    protected function fillUserInfo($userInfo)
    {
        foreach ($this->userSocialInfo as $key => $val) {
            $this->userSocialInfo[$key] = ($userInfo[$this->fieldsAlias[$key]] ?: null);
        }
        Session::getSession()->user = $this->userSocialInfo;//todo delete!
    }

    public function getName()
    {
        return $this->userSocialInfo['name'] ?: null;
    }

    public function getSocialInfo()
    {
        return $this->userSocialInfo['socialInfo'] ?: null;
    }

    public function getSocialId()
    {
        return $this->userSocialInfo['socialId'] ?: null;
    }

    public function getEmail()
    {
        return $this->userSocialInfo['email'] ?: null;
    }

    public function getAllUserInfo(){
        return $this->userSocialInfo ?: null;
    }

}
