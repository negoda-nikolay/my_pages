<?php
namespace Goodevening\Auth\SocialAuth;

class SocialAuth
{
    public static $adapters = [];
    private $links = [];

    /**
     * Make initializations of class social providers
     *
     * @param array $adapterConfig
     * @throws \Exception
     */
    public function __construct(array $adapterConfig)
    {
        if (!is_array($adapterConfig)) {
            throw new \Exception("Expected array with 'adapter_name' => 'params'");
        }
        foreach ($adapterConfig as $adapter => $configs) {
            $adapterName = 'Goodevening\Auth\SocialAuth\\' . ucfirst($adapter);
            self::$adapters[$adapter] = new $adapterName($configs);
        }
    }

    /**
     * Bind this links with href your social button
     *
     * @return array
     */
    public function getAllAuthUri()
    {
        foreach (self::$adapters as $providerName => $classProvider) {
            $this->links[$providerName] = $classProvider->getAuthUri();
        }
        return $this->links;
    }
}