<?php

namespace Goodevening\Auth;

use Goodevening\Auth\SocialAuth\SocialAuth;
use Goodevening\Db\Tables\Auth as AuthTable;
use Goodevening\Session\Session;

class Auth
{
    private $session;

    public function __construct()
    {
        $this->session = Session::getSession();
    }

    /**
     * @param array $fields
     * @return bool|void
     * @throws \Exception
     */
    public function authenticate(array $fields)
    {
        if (isset($_GET['provider'])) {
            $providerName = $_GET['provider'];

            //get all social adapters
            $adapters = SocialAuth::$adapters;
            if (array_key_exists($providerName, $adapters)) {
                $adapter = $adapters[$providerName];
                $adapter->authenticate();

                return true;
            }
        } elseif (isset($fields['password']) && isset($fields['email'])) {
            return $this->authViaPassword($fields);
        } else {
            throw new \Exception("Can't authenticate user. Use email and password or social links");
        }
        return false;
    }

    /**
     * Return an array of social links to confirm registration
     *
     * @return array
     */
    public function getSocialUris()
    {
        //todo this $adaptersParams must be in auth.conf.php file
        $adaptersParams = [
            'google' => [
                'clientId' => '590391918931-dkh802j58c92fptg7treo0jc00ip7jdl.apps.googleusercontent.com',
                'clientSecret' => 'GnR_Fsvp_i2hKhR59b-j-VgM',
                'redirectUri' => 'http://goodeveningproj.lo.com/auth/provider/google',
            ],
            'yandex' => [
                'clientId' => '77cbe17adcc540369a4399fbdbcb327a',
                'clientSecret' => '84b494e6091648d394e15e82186a4461',
                'redirectUri' => 'http://goodeveningproj.lo/auth?provider=yandex',
            ],
            'mailru' => [
                'clientId' => 741164,
                'clientSecret' => 'af5e1840450fbd5359bd8ba02beef67c',
                'redirectUri' => 'http://goodeveningproj.lo/auth?provider=mailru',
            ],
        ];

        $adapters = new SocialAuth($adaptersParams);
        return $adapters->getAllAuthUri();
    }

    /**
     * @param array $param
     * @return bool
     */
    private function authViaPassword(array $param)
    {
        $user = AuthTable::find(['email' => $param['email']], ['id', 'name', 'auth_via',
            'email', 'role', 'login', 'password', 'sid'])[0];

        // use password_hash() before writing password in DB
        //$passwordHash = password_hash($param['password'], PASSWORD_DEFAULT);
        if ($user && password_verify($param['password'], $user->password) && $user->email === $param['email']) {
            return $this->login($user);
        } else {
            echo 'Wrong password or email';//for test
            return false;
        }
    }

    /**
     * @param AuthTable $user
     * @return bool
     */
    private function login(AuthTable $user)
    {
        $userInfo['name'] = $user->name;
        $userInfo['email'] = $user->email;
        $userInfo['login'] = $user->login;
        $userInfo['role'] = 'user';

        $this->session->user = $userInfo;
        return true;
    }

    /**
     * @return bool
     */
    public function isLogged()
    {
        if (isset($this->session->user)) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function logout()
    {
        unset($this->session->user);
        return $this->session->destroy();
    }
}
