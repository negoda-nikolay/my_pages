<?php
namespace APP;
/**
 * Created by PhpStorm.
 * User: zyuzka
 * Date: 11.01.2016
 * Time: 23:55
 */
class Route
{

    public static function start()
    {
        $controller = 'home';
        $action = 'Index';
        $dirPages = BootConfig::getInstance()->getConfig('Dirs', 'pages');
        $uri = rtrim($_SERVER['REQUEST_URI'], '/');
        $uri = explode('/', $uri);

        if (isset($uri[1])) {
            $controller = $uri[1];
        }

        if (isset($uri[2])) {
            $action = $uri[2];
        }

        $filePath = $dirPages . $controller . '.phtml';

        if (!file_exists($filePath)) {
            $controller = '404';
        }
        View::render($controller);
    }

}