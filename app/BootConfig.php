<?php
/**
 * Created by PhpStorm.
 * User: phil
 * Date: 14.01.16
 * Time: 11:15
 */

namespace APP;


final class BootConfig
{

    protected static $instance;
    public $configFile = [];

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }


    public function registration($name, $path)
    {
        $newConfigFile = require_once($path);
        self::getInstance()->configFile[$name] = $newConfigFile;
    }

    public function getConfig($nameConfig, $key = null)
    {
        if (array_key_exists($nameConfig, self::getInstance()->configFile)) {
            if (null !== $key) {
                return self::getInstance()->configFile[$nameConfig][$key];
            } else {
                return self::getInstance()->configFile[$nameConfig];
            }
        } else {
            return false;
        }
    }

    public function __construct()
    {
    }

    public function __clone()
    {
    }
}