<?php
namespace APP;
/**
 * Created by PhpStorm.
 * User: zyuzka
 * Date: 12.01.2016
 * Time: 0:15
 */

class View
{
    public static function render($contentView)
    {
        ob_start();
        require(BootConfig::getInstance()->getConfig('Dirs', 'pages') . $contentView . '.phtml');
        $content = ob_get_clean();
        self::generate($content);
    }

    private static function generate($content)
    {
        require(BootConfig::getInstance()->getConfig('Dirs', 'layout') . 'layout.phtml');
    }
}