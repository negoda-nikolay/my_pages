<?php
/**
 * Created by PhpStorm.
 * User: phil
 * Date: 19.01.16
 * Time: 12:39
 */

namespace APP\controllers;

use APP\Controller;

class ControllerHome extends Controller
{
    function actionIndex()
    {
        View::render($controller);
    }
}