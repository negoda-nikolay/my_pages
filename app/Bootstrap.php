<?php
/**
 * Created by PhpStorm.
 * User: phil
 * Date: 14.01.16
 * Time: 12:16
 */

namespace APP;


class Bootstrap
{
    public static function start()
    {
        $config = BootConfig::getInstance();
        $config->registration('Dirs', 'config/dirs.php');
        Route::start();
    }
}