<?php
namespace Application\Controllers;

use Goodevening\Controller\Controller;

class Err404Controller extends Controller
{

    public function indexAction()
    {
        $this->view->render("404.phtml","layout.phtml", null);
    }
}