<?php
namespace Application\Controllers;

use Goodevening\Controller\Controller;

class LocationsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function indexAction()
    {
        $this->view->render('locations.phtml', 'layout.phtml', null);
    }
}