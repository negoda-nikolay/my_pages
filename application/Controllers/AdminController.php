<?php
namespace Application\Controllers;

use Goodevening\Controller\Controller;

class AdminController extends Controller
{
    public function indexAction()
    {
        $this->view->render("admin.phtml", "layout.phtml", null);
    }
}