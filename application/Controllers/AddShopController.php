<?php
namespace Application\Controllers;

use Goodevening\Controller\Controller;

class AddShopController extends Controller
{
    public function __construct(){
        parent::__construct();
    }
    public function indexAction()
    {
        $this->view->render("add-shop.phtml","layout.phtml", null);
    }
}