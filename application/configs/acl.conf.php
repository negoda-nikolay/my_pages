<?php
return [
/*    'allow' => [
        'guest' => [
            'LocationsController' => [
                'showLocation',
            ],
        ],
        'shopowner' => [
            'LocationsController' => [
                'showLocation',
            ],
        ],
        'user' => [
            'LocationsController' =>
                ['showLocation',
                ],
        ],
        'admin' => [
            'myResouce' => [],
        ],
    ],
    */
    'deny' => [
        'guest' => [
            'AdminController' => [
                'index',
                'create',
                'delete',
                'rename',
                'lock',
            ],
            'AddShopController' => [
                'index',
                'add',
                'delete',
                'rename',
            ],
            'LocationsController' => [
                'editLocation',
            ],
        ],
        'user' => [
            'AdminController' => [
                'index',
                'create',
                'delete',
                'rename',
                'lock',
            ],
            'AddShopController' => [
                'index',
                'add',
                'delete',
                'rename',
            ],
            'SomeController' => [
                'SomePrivilege',
                'Otherprivilege',
            ],
        ],
        'admin' => [
            'myResouce' => [
                'edit',
            ],
        ],
    ],
];