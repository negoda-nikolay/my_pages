<?php
return [
    'add-shop/([a-z]+)' => 'addShop/$1',
    'add-shop' => 'addShop/index',
    'admin/([a-z]+)' => 'admin/$1',
    'admin' => 'admin/index',
    '' => 'locations/index',
];